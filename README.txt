Application uses in-memory h2 database.
Application is spring-boot based. In order to run it just run Application.class
It also uses latest hibernate version. I also put swagger inside.
Provided integration tests as well alongside with SOAP and REST controllers.