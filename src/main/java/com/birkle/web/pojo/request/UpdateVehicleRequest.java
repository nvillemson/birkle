package com.birkle.web.pojo.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateVehicleRequest {

    @NotNull
    @Min(1)
    private Long id;

    @Size(min = 3, max = 30)
    private String brand;

    @Size(min = 3, max = 30)
    private String model;

    @Size(min = 3, max = 20)
    private String vehicleType;

    @Size(min = 3, max = 3)
    private String plateCountryIso;

    @Size(min = 2, max = 20)
    private String plateNumber;

    @Size(min = 10, max = 17)
    private String vinNumber;

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate creationDate;

    @Size(min = 3, max = 3)
    private String manufacturerCountryIso;
}
