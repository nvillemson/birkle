package com.birkle.web.pojo.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddVehicleRequest {

    @NotNull
    @Size(min = 1, max = 30)
    private String brand;

    @NotNull
    @Size(min = 1, max = 30)
    private String model;

    @NotNull
    @Size(min = 1, max = 20)
    private String vehicleType;

    @NotNull
    @Size(min = 3, max = 3)
    private String plateCountryIso;

    @NotNull
    @Size(min = 1, max = 20)
    private String plateNumber;

    @NotNull
    @Size(min = 10, max = 17)
    private String vinNumber;

    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate creationDate;

    @NotNull
    @Size(min = 3, max = 3)
    private String manufacturerCountryIso;

}
