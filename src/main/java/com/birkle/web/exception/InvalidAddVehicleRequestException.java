package com.birkle.web.exception;

public class InvalidAddVehicleRequestException extends RuntimeException {

    public InvalidAddVehicleRequestException(String message) {
        super(message);
    }

}
