package com.birkle.web.service;

import com.birkle.persistence.additional.Brand;
import com.birkle.persistence.additional.VehicleType;
import com.birkle.persistence.model.Car;
import com.birkle.persistence.repository.VehicleRepository;
import com.birkle.web.exception.InvalidAddVehicleRequestException;
import com.birkle.web.exception.VehicleNotFoundException;
import com.birkle.web.pojo.request.AddVehicleRequest;
import com.birkle.web.pojo.request.UpdateVehicleRequest;
import com.birkle.web.pojo.response.IdResponse;
import com.birkle.web.pojo.response.VehicleResponse;
import com.birkle.web.pojo.response.VehiclesResponse;
import localhost._8080.addvehicles.AddVehiclesResponse;
import localhost._8080.addvehicles.Vehicle;
import localhost._8080.addvehicles.VehicleId;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleService {

    private VehicleRepository vehicleRepository;

    public VehicleService(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public IdResponse addVehicle(AddVehicleRequest req) {
        Brand brand = validateBrand(req.getBrand());
        VehicleType type = validateType(req.getVehicleType());
        return vehicleRepository.saveVehicle(new Car(null, brand, req.getModel(), type, req.getPlateCountryIso(), req.getPlateNumber(), req.getVinNumber(),
                req.getCreationDate(), null, req.getManufacturerCountryIso()));
    }


    public VehiclesResponse listVehicles(long from, long to) {
        List<Car> cars = vehicleRepository.listVehicles(from, to);
        List<VehicleResponse> vehicles = new ArrayList<>();
        cars.forEach(car -> vehicles.add(new VehicleResponse(car.getId(), car.getBrand().name(), car.getModel(), car.getVehicleType().name(), car.getPlateCountryIso(),
                car.getPlateNumber(), car.getVinNumber(), car.getCreationDate(), car.getDeletionDate(), car.getManufacturerCountryIso())));
        return new VehiclesResponse(vehicles);
    }

    public VehicleResponse getVehicle(long vehicleId) {
        Car car = getVehicleById(vehicleId);
        return new VehicleResponse(car.getId(), car.getBrand().name(), car.getModel(), car.getVehicleType().name(), car.getPlateCountryIso(),
                car.getPlateNumber(), car.getVinNumber(), car.getCreationDate(), car.getDeletionDate(), car.getManufacturerCountryIso());
    }

    public void deleteVehicle(long vehicleId) {
        Car car = getVehicleById(vehicleId);
        vehicleRepository.deleteVehicle(car);
    }

    public void updateVehicle(UpdateVehicleRequest req) {
        Car car = getVehicleById(req.getId());

        if (!StringUtils.isEmpty(req.getBrand())) {
            Brand brand = validateBrand(req.getBrand());
            car.setBrand(brand);
        }
        if (!StringUtils.isEmpty(req.getVehicleType())) {
            VehicleType type = validateType(req.getVehicleType());
            car.setVehicleType(type);
        }
        if (!StringUtils.isEmpty(req.getVinNumber())) {
            car.setVinNumber(req.getVinNumber());
        }
        if (!StringUtils.isEmpty(req.getModel())) {
            car.setModel(req.getModel());
        }
        if (!StringUtils.isEmpty(req.getPlateCountryIso())) {
            car.setPlateCountryIso(req.getPlateCountryIso());
        }
        if (!StringUtils.isEmpty(req.getPlateNumber())) {
            car.setPlateNumber(req.getPlateNumber());
        }
        if (!StringUtils.isEmpty(req.getManufacturerCountryIso())) {
            car.setManufacturerCountryIso(req.getManufacturerCountryIso());
        }
        if (req.getCreationDate() != null) {
            car.setCreationDate(req.getCreationDate());
        }

        vehicleRepository.update(car);
    }

    private Car getVehicleById(long vehicleId) {
        Car car = vehicleRepository.getVehicle(vehicleId);
        if (car == null) {
            throw new VehicleNotFoundException("Vehicle nr " + vehicleId + "does not exist.");
        }
        return car;
    }

    private Brand validateBrand(String input) {
        try {
            return Brand.valueOf(input);
        } catch (IllegalArgumentException e) {
            throw new InvalidAddVehicleRequestException(
                    "Invalid value for enum Brand " + e.getMessage() + ".");
        }
    }

    private VehicleType validateType(String input) {
        try {
            return VehicleType.valueOf(input);
        } catch (IllegalArgumentException e) {
            throw new InvalidAddVehicleRequestException(
                    "Invalid value for enum VehicleType " + e.getMessage() + ".");
        }
    }

    public AddVehiclesResponse bulkAddVehicles(List<Vehicle> vehicles) {
        List<Car> cars = new ArrayList<>();
        vehicles.forEach(vehicle -> {
            Brand brand = validateBrand(vehicle.getBrand());
            VehicleType type = validateType(vehicle.getVehicleType());
            LocalDate creationDate = LocalDate.of(vehicle.getCreationDate().getYear(), vehicle.getCreationDate().getMonth(), vehicle.getCreationDate().getDay());
            cars.add(new Car(null, brand, vehicle.getModel(), type, vehicle.getPlateCountryIso(), vehicle.getPlateNumber(), vehicle.getVinNumber(),
                    creationDate, null, vehicle.getManufacturerCountryIso()));
        });

        vehicleRepository.bulkSave(cars);

        AddVehiclesResponse resp = new AddVehiclesResponse();
        cars.forEach(car -> {
                    VehicleId vehicleId = new VehicleId();
                    vehicleId.setId(car.getId());
                    vehicleId.setPlateNumber(car.getPlateNumber());
                    resp.getVehicleIds().add(vehicleId);
        });
        return resp;
    }
}
