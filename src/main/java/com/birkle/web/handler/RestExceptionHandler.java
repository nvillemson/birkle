package com.birkle.web.handler;

import com.birkle.web.exception.InvalidAddVehicleRequestException;
import com.birkle.web.exception.VehicleNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings("unused")
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidAddVehicleRequestException.class)
    protected ResponseEntity<String> handleVehicleRequestException(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(VehicleNotFoundException.class)
    protected ResponseEntity<String> handleVehicleNotFoundException(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

}
