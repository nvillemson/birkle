package com.birkle.web.controller;

import com.birkle.web.service.VehicleService;
import localhost._8080.addvehicles.AddVehiclesRequest;
import localhost._8080.addvehicles.AddVehiclesResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class VehiclesEndpoint {

    private VehicleService vehicleService;

    public VehiclesEndpoint(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PayloadRoot(namespace = "http://localhost:8080/addVehicles", localPart = "addVehiclesRequest")
    @ResponsePayload
    public AddVehiclesResponse bulkAddVehicles(@RequestPayload AddVehiclesRequest req) {
        return vehicleService.bulkAddVehicles(req.getVehicles());
    }

}
