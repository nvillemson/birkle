package com.birkle.web.controller;

import com.birkle.web.pojo.request.AddVehicleRequest;
import com.birkle.web.pojo.request.UpdateVehicleRequest;
import com.birkle.web.pojo.response.IdResponse;
import com.birkle.web.pojo.response.VehicleResponse;
import com.birkle.web.pojo.response.VehiclesResponse;
import com.birkle.web.service.VehicleService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
@RequestMapping("/vehicle")
public class VehicleController {

    private VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping("/add-vehicle")
    private @ResponseBody IdResponse addVehicle(@Valid @RequestBody AddVehicleRequest req) {
        return vehicleService.addVehicle(req);
    }

    @GetMapping("/list-vehicles")
    private @ResponseBody VehiclesResponse listVehicles(@RequestParam("from") long from, @RequestParam("to") long to) {
        return vehicleService.listVehicles(from, to);
    }

    @GetMapping("/get-vehicle")
    private @ResponseBody VehicleResponse listVehicles(@RequestParam("vehicleId") long vehicleId) {
        return vehicleService.getVehicle(vehicleId);
    }

    @DeleteMapping("/delete-vehicle")
    @ResponseStatus(HttpStatus.OK)
    private void deleteVehicle(@RequestParam("vehicleId") long vehicleId) {
        vehicleService.deleteVehicle(vehicleId);
    }

    @PostMapping("/update-vehicle")
    @ResponseStatus(HttpStatus.OK)
    private void updateVehicle(@Valid @RequestBody UpdateVehicleRequest req) {
        vehicleService.updateVehicle(req);
    }

}
