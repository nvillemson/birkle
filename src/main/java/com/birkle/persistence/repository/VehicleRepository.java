package com.birkle.persistence.repository;

import com.birkle.persistence.model.Car;
import com.birkle.web.pojo.response.IdResponse;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Transactional
public class VehicleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public IdResponse saveVehicle(Car car) {
        entityManager.unwrap(Session.class).persist(car);
        return new IdResponse(car.getId());
    }

    public List<Car> listVehicles(long from, long to) {
        CriteriaBuilder builder = entityManager.unwrap(Session.class).getCriteriaBuilder();
        CriteriaQuery<Car> query = builder.createQuery(Car.class);
        Root<Car> product = query.from(Car.class);
        query.where(builder.isNull(product.get("deletionDate")));
        query.select(product);

        return entityManager.unwrap(Session.class).createQuery(query)
                .setFirstResult((int) from)
                .setMaxResults((int) to)
                .getResultList();
    }

    public Car getVehicle(long vehicleId) {
        return entityManager.unwrap(Session.class).find(Car.class, vehicleId);
    }

    public void deleteVehicle(Car car) {
        car.setDeletionDate(LocalDateTime.now());
        entityManager.unwrap(Session.class).update(car);
    }

    public void update(Car car) {
        entityManager.unwrap(Session.class).update(car);
    }

    public List<Car> bulkSave(List<Car> cars) {
        cars.forEach(car -> entityManager.unwrap(Session.class).persist(car));
        entityManager.unwrap(Session.class).flush();
        return cars;
    }
}
