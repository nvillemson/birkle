package com.birkle.persistence.repository;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class BaseTestRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void clearDatabase() {
        entityManager.unwrap(Session.class).createNativeQuery("truncate table CAR").executeUpdate();
    }
}
