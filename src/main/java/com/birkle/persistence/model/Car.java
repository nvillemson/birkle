package com.birkle.persistence.model;

import com.birkle.persistence.additional.Brand;
import com.birkle.persistence.additional.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CAR")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "BRAND", nullable = false, columnDefinition = "VARCHAR(30)")
    private Brand brand;

    @Column(name = "MODEL", nullable = false, columnDefinition = "VARCHAR(30)")
    private String model;

    @Enumerated(EnumType.STRING)
    @Column(name = "VEHICLE_TYPE", nullable = false, columnDefinition = "VARCHAR(20)")
    private VehicleType vehicleType;

    @Column(name = "PLATE_COUNTRY_ISO", nullable = false, columnDefinition = "VARCHAR(3)")
    private String plateCountryIso;

    @Column(name = "PLATE_NUMBER", nullable = false, unique = true, columnDefinition = "VARCHAR(20)")
    private String plateNumber;

    @Column(name = "VIN_NUMBER", nullable = false, columnDefinition = "VARCHAR(17)")
    private String vinNumber;

    @Column(name = "CREATION_DATE", nullable = false, columnDefinition = "DATE")
    private LocalDate creationDate;

    @Column(name = "DELETION_DATE", columnDefinition = "DATETIME")
    private LocalDateTime deletionDate;

    @Column(name = "MANUFACTURER_COUNTRY_ISO", nullable = false, columnDefinition = "VARCHAR(3)")
    private String manufacturerCountryIso;

}
