package com.birkle.persistence.additional;

public enum VehicleType {

    HATCHBACK,
    SEDAN,
    MPV,
    SUV,
    CROSSOVER,
    COUPE,
    CONVERTIBLE

}
