package tests;

import com.birkle.web.controller.VehiclesEndpoint;
import general.AbstractTest;
import localhost._8080.addvehicles.AddVehiclesRequest;
import localhost._8080.addvehicles.AddVehiclesResponse;
import localhost._8080.addvehicles.Vehicle;
import localhost._8080.addvehicles.VehicleId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VehiclesEndpointTest extends AbstractTest {

    @Autowired
    private VehiclesEndpoint vehiclesEndpoint;

    @Test
    public void bulkUploadTestSuccess() throws DatatypeConfigurationException {
        AddVehiclesRequest req = new AddVehiclesRequest();
        req.getVehicles().add(produceVehicle("BMW", "X5", "SEDAN", "EST", "213-GFY",
                "5347352725765", "GER"));
        req.getVehicles().add(produceVehicle("VOLKSWAGEN", "POLO", "HATCHBACK", "GER", "253-GFY",
                "53473580725765", "FRA"));
        AddVehiclesResponse addVehiclesResponse = vehiclesEndpoint.bulkAddVehicles(req);
        List<VehicleId> vehicleIds = addVehiclesResponse.getVehicleIds();
        assertEquals(2, vehicleIds.size());
        assertEquals("213-GFY", vehicleIds.get(0).getPlateNumber());
        assertEquals("253-GFY", vehicleIds.get(1).getPlateNumber());
    }

    private Vehicle produceVehicle(String brand, String model, String vehicleType, String plateCountryIso, String plateNumber,
                                   String vinNumber, String manufacturerCountryIso) throws DatatypeConfigurationException {
        Vehicle veh1 = new Vehicle();
        veh1.setBrand(brand);
        veh1.setManufacturerCountryIso(manufacturerCountryIso);
        veh1.setModel(model);
        veh1.setPlateCountryIso(plateCountryIso);
        veh1.setPlateNumber(plateNumber);
        veh1.setVehicleType(vehicleType);
        veh1.setVinNumber(vinNumber);

        LocalDate localDate = LocalDate.of(2019, 4, 25);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
        veh1.setCreationDate(xmlGregorianCalendar);
        return veh1;
    }
}
