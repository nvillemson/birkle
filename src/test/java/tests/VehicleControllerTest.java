package tests;

import com.birkle.web.pojo.request.AddVehicleRequest;
import com.birkle.web.pojo.request.UpdateVehicleRequest;
import com.birkle.web.pojo.response.IdResponse;
import com.birkle.web.pojo.response.VehicleResponse;
import com.birkle.web.pojo.response.VehiclesResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.LocalDate;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class VehicleControllerTest extends AbstractTest {

    @Test
    public void testAddVehicleTestSuccess() throws IOException {
            createVehicle("BMW", "X5", "SUV", "EST", "133-QWE", "12345671234567890",
                    "GER");
    }

    @Test
    public void testAddVehicleTestBadRequest() throws IOException {
        // wrong manufacturer country
        LocalDate creationDate = LocalDate.now().plusYears(7);
        AddVehicleRequest request = new AddVehicleRequest("BMW", "X5", "SUV", "EST", "126-QWE",
                "12345671234567890",creationDate, "test");
        HttpEntity<String> entity = produceVehicleEntity(request);
        ResponseEntity<String> response = template.postForEntity(addVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // wrong mark
        request = new AddVehicleRequest("tere", "X5", "SUV", "EST", "153-QWE",
                "12345671234567890",creationDate, "EST");
        entity = produceVehicleEntity(request);
        response = template.postForEntity(addVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals(response.getBody(), "Invalid value for enum Brand No enum constant com.birkle.persistence.additional.Brand.tere.");

        // wrong type
        request = new AddVehicleRequest("MERCEDES", "X5", "test", "EST", "122-QWE",
                "12345671234567890",creationDate, "EST");
        entity = produceVehicleEntity(request);
        response = template.postForEntity(addVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals(response.getBody(), "Invalid value for enum VehicleType No enum constant com.birkle.persistence.additional.VehicleType.test.");
    }

    @Test
    public void getUpdateVehicleTestSuccess() throws IOException {
        IdResponse vehicleId = createVehicle("BMW", "X5", "SUV", "EST", "149-QWE", "12345679234567890",
                "GER");

        ResponseEntity<String> response = template.getForEntity(getVehicle + "?vehicleId=" + vehicleId.getId(), String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        VehicleResponse resp = mapper.readValue(response.getBody(), VehicleResponse.class);
        assertEquals("BMW", resp.getBrand());
        assertEquals("149-QWE", resp.getPlateNumber());
        assertEquals("12345679234567890", resp.getVinNumber());
        assertEquals("SUV", resp.getVehicleType());

        HttpEntity<String> entity = produceUpdateVehicleEntity(new UpdateVehicleRequest(vehicleId.getId(), "OPEL", null, "SEDAN", null,
                null, null, null, null));
        response = template.postForEntity(updateVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getVehicle + "?vehicleId=" + vehicleId.getId(), String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        resp = mapper.readValue(response.getBody(), VehicleResponse.class);
        assertEquals("OPEL", resp.getBrand());
        assertEquals("149-QWE", resp.getPlateNumber());
        assertEquals("12345679234567890", resp.getVinNumber());
        assertEquals("SEDAN", resp.getVehicleType());
    }

    @Test
    public void getUpdateVehicleTestBadRequest() throws IOException {
        IdResponse vehicleId = createVehicle("MERCEDES", "X5", "SUV", "EST", "743-QWE", "12345679234567890",
                "GER");

        // wrong brand
        HttpEntity<String> entity = produceUpdateVehicleEntity(new UpdateVehicleRequest(vehicleId.getId(), "HELLO", null, "SEDAN", null,
                null, null, null, null));
        ResponseEntity<String> response = template.postForEntity(updateVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // wrong type
        entity = produceUpdateVehicleEntity(new UpdateVehicleRequest(vehicleId.getId(), "OPEL", null, "HELLO", null,
                null, null, null, null));
        response = template.postForEntity(updateVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // no such car
        entity = produceUpdateVehicleEntity(new UpdateVehicleRequest(123342l, "OPEL", null, "SEDAN", null,
                null, null, null, null));
        response = template.postForEntity(updateVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void deleteListVehiclesTestSuccess() throws IOException {
        IdResponse vehicleId1 = createVehicle("AUDI", "X5", "SUV", "EST", "543-QWE", "12345679234567890",
                "GER");
        IdResponse vehicleId2 = createVehicle("VOLKSWAGEN", "X5", "SUV", "EST", "183-QWE", "12345679234567890",
                "GER");

        ResponseEntity<String> response = template.getForEntity(listVehicles + "?from=0&to=100", String.class);
        VehiclesResponse resp = mapper.readValue(response.getBody(), VehiclesResponse.class);
        assertEquals(2, resp.getVehicles().size());

        template.delete(deleteVehicle + "?vehicleId=" + vehicleId1.getId());

        response = template.getForEntity(listVehicles + "?from=0&to=100", String.class);
        resp = mapper.readValue(response.getBody(), VehiclesResponse.class);
        assertEquals(1, resp.getVehicles().size());
        assertEquals("183-QWE", resp.getVehicles().get(0).getPlateNumber());
    }

}
