package general;

import com.birkle.Application;
import com.birkle.persistence.repository.BaseTestRepository;
import com.birkle.web.pojo.request.AddVehicleRequest;
import com.birkle.web.pojo.request.UpdateVehicleRequest;
import com.birkle.web.pojo.response.IdResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    protected String addVehicle;
    protected String listVehicles;
    protected String getVehicle;
    protected String deleteVehicle;
    protected String updateVehicle;

    @Value("${server.port}")
    private int serverPort;

    protected ObjectMapper mapper;

    @Autowired
    private BaseTestRepository baseTestRepository;

    @Autowired
    protected TestRestTemplate template;

    @Before
    public void setUp() throws MalformedURLException {
        URL base = new URL("http://localhost:" + serverPort + "/");
        addVehicle = base.toString() + "/vehicle/add-vehicle";
        listVehicles = base.toString() + "/vehicle/list-vehicles";
        getVehicle = base.toString() + "/vehicle/get-vehicle";
        deleteVehicle = base.toString() + "/vehicle/delete-vehicle";
        updateVehicle = base.toString() + "/vehicle/update-vehicle";
        baseTestRepository.clearDatabase();
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    protected HttpEntity<String> produceVehicleEntity(AddVehicleRequest req) throws JsonProcessingException {
        String json = mapper.writeValueAsString(req);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected HttpEntity<String> produceUpdateVehicleEntity(UpdateVehicleRequest req) throws JsonProcessingException {
        String json = mapper.writeValueAsString(req);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected IdResponse createVehicle(String brand, String model, String vehicleType, String plateCountryIso, String plateNumber,
                                       String vinNumber, String manufacturerCountryIso) throws IOException {
        LocalDate creationDate = LocalDate.now().plusYears(7);
        AddVehicleRequest request = new AddVehicleRequest(brand, model, vehicleType, plateCountryIso, plateNumber, vinNumber, creationDate, manufacturerCountryIso);
        HttpEntity<String> entity = produceVehicleEntity(request);
        ResponseEntity<String> response = template.postForEntity(addVehicle, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        return mapper.readValue(response.getBody(), IdResponse.class);
    }
}
